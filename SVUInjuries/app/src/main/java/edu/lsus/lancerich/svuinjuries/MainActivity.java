package edu.lsus.lancerich.svuinjuries;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Activity context = MainActivity.this;
    private EditText vic_inj_NormalText;
    private FloatingActionButton finishedButton;
    private EditText medicalFacility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final LinearLayout injuryList = (LinearLayout) findViewById(R.id.injuryList);
        final EditText injuries = (EditText) findViewById(R.id.injuries);
        Button addInjury = (Button) findViewById(R.id.addInjury);
        CheckBox medicalHelp = (CheckBox) findViewById(R.id.medicalHelp);
        CheckBox medicalForm = (CheckBox) findViewById(R.id.medicalForm);
        FloatingActionButton finished = (FloatingActionButton) findViewById(R.id.finished);

        registerViews();

        addInjury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                injuryList.addView(context.getLayoutInflater().inflate(R.layout.injury_list, injuryList, false));
            }
        });
    }

    private void registerViews() {
        vic_inj_NormalText = (EditText) findViewById(R.id.injuries);
        medicalFacility = (EditText) findViewById(R.id.medicalFacility);
        // TextWatcher would let us check validation error on the fly
        vic_inj_NormalText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.hasText(vic_inj_NormalText);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        medicalFacility.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Validation.hasText(medicalFacility);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        finishedButton = (FloatingActionButton) findViewById(R.id.finished);
        finishedButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                /*
                Validation class will check the error and display the error on respective fields
                but it won't resist the form submission, so we need to check again before submit
                 */
                if ( checkValidation () )
                    submitForm();
                else
                    Toast.makeText(MainActivity.this, "Form contains error", Toast.LENGTH_LONG).show();
                }
        });
    }

    private void submitForm() {
        // Submit your form here. your form is valid
        Toast.makeText(this, "Next ...", Toast.LENGTH_LONG).show();
    }

    private boolean checkValidation() {
        boolean ret = true;
        boolean ret_two = true;

        if (!Validation.hasText(vic_inj_NormalText) || !Validation.hasText(medicalFacility))
            ret = false;
        return ret;
    }
}

